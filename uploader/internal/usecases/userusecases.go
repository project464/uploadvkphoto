package usecases

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"time"
	"uploader/internal/dataproviders/psql"
)

func (uc *UseCases) UpdateUser(user *psql.User) error {
	// В случае каких либо ошибок система берет значения из уже существующей записи.
	_, err := uc.psqlprovider.UpdateUser(*user)
	if err != nil {
		uc.logger.Error(err)
		return err
	}
	return nil
}

func (uc *UseCases) GetUser(user *psql.User) (*psql.User, error) {
	// В случае каких либо ошибок система берет значения из уже существующей записи.
	userFull, err := uc.psqlprovider.GetUser(user)
	if err != nil {
		uc.logger.Error(err)
		return nil, err
	}
	return userFull, nil
}

func (uc UseCases) CheckLogin(login, password string) (expiration time.Time, hashedToken string, err error) {
	if login == "" || password == "" {
		err = fmt.Errorf("Необходимо указать логин и пароль!")
		return
	}
	hash := md5.Sum([]byte(password))
	hashedPass := hex.EncodeToString(hash[:])
	user, err := uc.psqlprovider.LoginUser(
		psql.User{
			Login: login,
		})
	if err != nil {
		err = fmt.Errorf("Вы ввели неверный логин или пароль!")
		return
	}
	if hashedPass != user.HashedPassword {
		err = fmt.Errorf("Вы ввели неверный логин или пароль!")
		return
	}
	time64 := time.Now().Unix()
	timeInt := string(time64)
	token := login + password + timeInt
	hashToken := md5.Sum([]byte(token))
	hashedToken = hex.EncodeToString(hashToken[:])
	uc.cache[hashedToken] = user
	livingTime := 60 * time.Minute
	expiration = time.Now().Add(livingTime)
	uc.logger.Info("Error: ", err)
	return
}
func (uc UseCases) SignUp(name, surname, login, password, password2 string) (createdUser *psql.User, err error) {
	uc.logger.Info("Use cases signup")
	if name == "" || surname == "" || login == "" || password == "" {
		err = fmt.Errorf("Все поля должны быть заполнены!")
		return
	}
	if password2 != password {
		err = fmt.Errorf("Пароли не совпадают!")
		return
	}
	hash := md5.Sum([]byte(password))
	hashedPass := hex.EncodeToString(hash[:])
	createdUser, err = uc.psqlprovider.CreateUser(
		&psql.User{
			Name:           name,
			Surname:        surname,
			Login:          login,
			HashedPassword: hashedPass,
		})
	if err != nil {
		fmt.Errorf("Ошибка создания пользователя: %v", err)
		return
	}
	return
}
