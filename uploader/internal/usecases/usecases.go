package usecases

import (
	"context"
	"go.uber.org/zap"
	"io"
	"time"
	"uploader/internal/dataproviders/psql"
)

type UseCases struct {
	ctx          context.Context
	psqlprovider psql.IPostgresProvider
	logger       *zap.SugaredLogger
	cache        map[string]psql.User
}

type IUseCases interface {
	CreatePost(user psql.User, postdatetime, text string, daily, monthly bool, file io.Reader) error
	GetUserPostList(user psql.User) ([]psql.Post, error)
	GetPost(psql.Post) ([]psql.Post, error)
	UpdatePost(id, postdatetime, text string, daily, monthly bool, file io.Reader) error
	UpdateUser(user *psql.User) error
	GetUser(user *psql.User) (*psql.User, error)
	DeletePost(post *psql.Post) error
	CheckLogin(login, password string) (time.Time, string, error)
	SignUp(name, surname, login, password, password2 string) (*psql.User, error)
}

func NewUseCases(
	ctx context.Context,
	postgresdb psql.IPostgresProvider,
	sugaredlogger *zap.SugaredLogger,
	cache map[string]psql.User,
) IUseCases {
	return &UseCases{
		ctx:          ctx,
		psqlprovider: postgresdb,
		logger:       sugaredlogger,
		cache:        cache,
	}
}
