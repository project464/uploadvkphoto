package entrypoints

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"html/template"
	"net/http"
	"path/filepath"
	"sort"
	"uploader/internal/dataproviders/psql"
)

type pageFunction func(w http.ResponseWriter, r *http.Request, p httprouter.Params)

func (ep *Entrypoints) userProfile(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	ep.logger.Info("Entering user profile entrypoint")
	token, err := readCookie("token", r)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	cacheUser, ok := ep.cache[token]
	if !ok {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	user, err := ep.usecases.GetUser(&psql.User{Id: cacheUser.Id})
	//ep.logger.Info("Post list:", postlist)
	if err != nil {
		fmt.Fprintf(w, fmt.Sprint(err))
	}
	postlist, err := ep.usecases.GetUserPostList(psql.User{Id: cacheUser.Id})
	ep.logger.Debug("Post list:", postlist)
	if err != nil {
		fmt.Fprintf(w, fmt.Sprint(err))
	}
	//сортируем в обратном порядке
	sort.Slice(postlist, func(i, j int) bool {
		return postlist[j].TimeOfPost.Before(postlist[i].TimeOfPost)
	})
	sp := filepath.Join("static", "userprofile.html")
	tmpl := template.Must(template.ParseFiles(sp))
	type Data struct {
		User psql.User
		List []psql.Post
	}
	ep.logger.Debug(user)
	ep.logger.Debug(postlist)
	err = tmpl.ExecuteTemplate(w, "user", user)
	err = tmpl.ExecuteTemplate(w, "postlist", postlist)

	if err != nil {
		fmt.Fprintf(w, fmt.Sprint(err))
	}
}

func (ep Entrypoints) profile(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	ep.logger.Info("entrypoint Profile")

	name := r.FormValue("name")
	surname := r.FormValue("surname")
	vkuser := r.FormValue("vkuser")
	vkpass := r.FormValue("vkpass")
	vkgroups := r.FormValue("vkgroups")
	telegramgroups := r.FormValue("telegramgroups")
	telegramtoken := r.FormValue("telegramtoken")

	userid, err := ep.userIdAsIntFromAuthByParams(p)
	if ep.checkErrorAndGoToProfilePage(err, "error parsing id", rw, r, p, ep.profilePage) {
		return
	}

	user := &psql.User{
		Id:             userid,
		Name:           name,
		Surname:        surname,
		VKuser:         vkuser,
		VKpass:         vkpass,
		VKgroups:       vkgroups,
		TelegramToken:  telegramtoken,
		TelegramGroups: telegramgroups,
	}

	err = ep.usecases.UpdateUser(user)
	ep.checkErrorAndGoToProfilePage(err, "error update user", rw, r, p, ep.profilePage)

	ep.profilePage(rw, r, p)
	return
}

func (ep Entrypoints) profilePage(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
	ep.logger.Info("entrypoint ProfilePage")
	lp := filepath.Join("static", "profile.html")
	tmpl, err := template.ParseFiles(lp)
	if err != nil {
		ep.logger.Error(err)
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	errorMessage := p.ByName("errormessage")
	ep.logger.Info("errormessage:", errorMessage)
	userid, err := ep.userIdAsIntFromAuthByParams(p)
	if err != nil {
		ep.logger.Info("DB error:", err)
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	currentUser, err := ep.usecases.GetUser(&psql.User{Id: userid})
	ep.logger.Info("User that came from db:", currentUser)
	if err != nil {
		ep.logger.Info("DB error:", err)
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
	err = tmpl.ExecuteTemplate(rw, "profile", currentUser)
	err = tmpl.ExecuteTemplate(rw, "message", errorMessage)
	if err != nil {
		ep.logger.Info("Template error:", err)
		http.Error(rw, err.Error(), http.StatusBadRequest)
		return
	}
}
