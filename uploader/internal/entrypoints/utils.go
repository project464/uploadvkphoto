package entrypoints

import (
	"errors"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"net/url"
	"strconv"
)

func (ep Entrypoints) checkErrorAndGoToProfilePage(
	err error,
	message string,
	rw http.ResponseWriter,
	r *http.Request,
	p httprouter.Params,
	f pageFunction,
) bool {
	if err != nil {
		ep.logger.Error(message)
		errorMessage := httprouter.Param{Key: "errormessage", Value: message}
		p := append(p, errorMessage)
		f(rw, r, p)
		return true
	}
	return false
}

func (ep Entrypoints) userIdAsIntFromAuthByParams(p httprouter.Params) (userid int, err error) {
	ep.logger.Info("Entering useridasintfromauthbyparams")
	ep.logger.Info("Params:", p)
	id := p.ByName("userid")
	ep.logger.Info("userid:", id)
	userid, err = strconv.Atoi(id)
	if err != nil {
		ep.logger.Error(err)
		return 0, err
	}
	return userid, nil
}

func readCookie(surname string, r *http.Request) (value string, err error) {
	if surname == "" {
		return value, errors.New("you are trying to read empty cookie")
	}
	cookie, err := r.Cookie(surname)
	if err != nil {
		return value, err
	}
	str := cookie.Value
	value, _ = url.QueryUnescape(str)
	return value, err
}
