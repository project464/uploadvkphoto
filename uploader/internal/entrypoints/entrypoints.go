package entrypoints

import (
	"context"
	"github.com/julienschmidt/httprouter"
	"go.uber.org/zap"
	"net/http"
	"uploader/internal/dataproviders/psql"
	"uploader/internal/usecases"
	"uploader/utils"
)

type Entrypoints struct {
	ctx      context.Context
	usecases usecases.IUseCases
	logger   *zap.SugaredLogger
	cache    map[string]psql.User
}

type IEntrypoints interface {
	SetupRoutes()
}

func NewEntrypoints(
	ctx context.Context,
	usecases usecases.IUseCases,
	sugaredlogger *zap.SugaredLogger,
	cache map[string]psql.User,
) IEntrypoints {
	return &Entrypoints{
		ctx:      ctx,
		usecases: usecases,
		logger:   sugaredlogger,
		cache:    cache,
	}
}

func (ep *Entrypoints) SetupRoutes() {
	r := httprouter.New()
	//Posts
	r.GET("/post", ep.Authorized(ep.userProfile))
	r.POST("/post", ep.Authorized(ep.postMethodSelector))

	//profile
	r.GET("/", ep.Authorized(ep.userProfile))
	r.GET("/editprofile", ep.Authorized(ep.profilePage))
	r.POST("/editprofile", ep.Authorized(ep.profile))

	//login
	r.GET("/login", func(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
		ep.loginPage(rw, "")
	})
	r.POST("/login", ep.login)
	r.GET("/logout", ep.logout)
	r.GET("/signup", func(rw http.ResponseWriter, r *http.Request, p httprouter.Params) {
		ep.signupPage(rw, "")
	})
	r.POST("/signup", ep.Signup)

	// Статика
	r.ServeFiles("/static/*filepath", http.Dir("static"))
	r.ServeFiles("/pics/*filepath", http.Dir("pics"))
	//  Запускаем сервер
	ep.logger.Info("app started: ", utils.Env.AddressAndPort)
	http.ListenAndServe(utils.Env.AddressAndPort, r)
}
