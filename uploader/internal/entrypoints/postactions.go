package entrypoints

import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strconv"
	"uploader/internal/dataproviders/psql"
)

func (ep *Entrypoints) postMethodSelector(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	ep.logger.Info("Entering post method selector")
	switch r.FormValue("func") {
	case "delete":
		ep.deletePost(w, r, p)
	default:
		ep.postPublication(w, r, p)
	}

}
func (ep *Entrypoints) deletePost(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	ep.logger.Info("Entering deletePost")
	stringId := r.FormValue("postId")
	userid, err := ep.userIdAsIntFromAuthByParams(p)
	ep.checkErrorAndGoToProfilePage(err, "wrong user id", w, r, p, ep.profilePage)
	ep.logger.Info("id: ", stringId)

	intId, err := strconv.Atoi(stringId)
	ep.checkErrorAndGoToProfilePage(err, "wrong user id", w, r, p, ep.profilePage)

	err = ep.usecases.DeletePost(&psql.Post{Id: intId, OwnerId: userid})
	ep.checkErrorAndGoToProfilePage(err, "problem to delete post", w, r, p, ep.profilePage)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (ep *Entrypoints) postPublication(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	ep.logger.Info("File Upload Endpoint Hit")
	ep.logger.Info(r)
	token, err := readCookie("token", r)
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	user, ok := ep.cache[token]
	if !ok {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	err = r.ParseMultipartForm(10 << 20)
	if err != nil {
		ep.logger.Info("Error Retrieving the File: size does not match")
		ep.logger.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("postFile")
	if err != nil {
		ep.logger.Info("Error Retrieving the File")
		ep.logger.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()
	text := r.FormValue("postText")
	postdatetime := r.FormValue("postDateTime")
	postDaily := r.FormValue("postDaily")
	postMonthly := r.FormValue("postMonthly")
	ep.logger.Info("Publication time ", postdatetime)
	ep.logger.Info("Uploaded File: ", handler.Filename)
	ep.logger.Info("File Size: ", handler.Size)
	ep.logger.Info("MIME Header: ", handler.Header)
	ep.logger.Info("text: ", text)
	ep.logger.Info("daily: ", postDaily)
	ep.logger.Info("monthly: ", postMonthly)
	daily := false
	monthly := false

	if postDaily == "on" {
		daily = true
	}
	if postMonthly == "on" {
		monthly = true
	}

	err = ep.usecases.CreatePost(user, postdatetime, text, daily, monthly, file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	ep.logger.Info("Redirecting to root")
	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func (ep *Entrypoints) postDeletePage(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	postId := r.FormValue("postId")
	ep.logger.Info(postId)
	sp := filepath.Join("static", "deletepost.html")
	page, err := ioutil.ReadFile(sp)
	if err != nil {
		fmt.Fprintf(w, fmt.Sprint(err))
	}
	fmt.Fprintf(w, string(page))
}
