package psql

import (
	"fmt"
	"gorm.io/gorm"
)

func (ip *PostgresProvider) CreateUser(mp *User) (*User, error) {
	ip.logger.Info("creating user: ", mp)
	var temp *User
	conn.Where("login=?", mp.Login).Find(&temp)
	if temp.Login != mp.Login {
		err := conn.Create(mp)
		if err.Error != nil {
			return nil, err.Error
		}
		return mp, nil
	}
	ip.logger.Info("Founded user: ", temp)
	return nil, fmt.Errorf("This login is forbiden")
}

func (ip *PostgresProvider) GetAllUsers() ([]User, error) {
	var mns []User
	var res *gorm.DB
	res = conn.Find(&mns)
	if res.Error != nil {
		return nil, res.Error
	}
	return mns, nil
}
func (ip *PostgresProvider) GetUser(mn *User) (mnr *User, err error) {
	ip.logger.Info("Getting users", mn)
	var res *gorm.DB
	res = conn.Find(&mnr, mn)
	if res.Error != nil {
		return nil, res.Error
	}
	return mnr, nil
}

func (ip *PostgresProvider) LoginUser(mn User) (User, error) {
	ip.logger.Info("Logging user before", mn)
	res := conn.First(&mn, "login = ?", mn.Login)
	if res.Error != nil {
		return User{}, res.Error
	}
	ip.logger.Info("Logging user after", mn)
	return mn, nil
}

func (ip *PostgresProvider) UpdateUser(mn User) (User, error) {
	ip.logger.Info("update")
	ip.logger.Info(mn)
	res := conn.Model(&mn).Updates(mn)
	if res.Error != nil {
		return User{}, res.Error
	}
	return mn, nil
}

func (ip *PostgresProvider) DeleteUser(n User) (User, error) {
	var mn User
	ip.logger.Info("delete")
	ip.logger.Info(n)
	res := conn.Delete(&mn, n)
	if res.Error != nil {
		//log.Println("delete fail")
		return User{}, res.Error
	}
	return mn, nil
}
