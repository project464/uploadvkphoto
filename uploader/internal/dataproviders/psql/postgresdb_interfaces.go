package psql

import (
	"context"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

var conn *gorm.DB

type PostgresProvider struct {
	ctx    context.Context
	logger *zap.SugaredLogger
}

type IPostgresProvider interface {
	InitDB()
	CreatePost(*Post) (*Post, error)
	DeletePost(Post) error
	UpdatePost(Post) (Post, error)
	GetPosts(Post) ([]Post, error)
	GetAllPosts(User) ([]Post, error)
	CreateUser(*User) (*User, error)
	DeleteUser(User) (User, error)
	UpdateUser(User) (User, error)
	GetUser(*User) (*User, error)
	LoginUser(User) (User, error)
	GetAllUsers() ([]User, error)
}

func NewPostgresProvider(
	ctx context.Context,
	sugaredlogger *zap.SugaredLogger,
) IPostgresProvider {
	return &PostgresProvider{
		ctx:    ctx,
		logger: sugaredlogger,
	}
}

func (ip *PostgresProvider) InitDB() {
	conn = initDBConn()
	ip.logger.Info("Database Connected")
	migrate(conn)
	ip.logger.Info("Database migrated")
}
