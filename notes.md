Пример подставновки переменной
```
{{ $lang := .lang }}
{{ range .users }}
  <form action="/{{ $lang }}/users" method="POST">
    <input type="text" name="Username" value="{{ .Username }}">
    <input type="text" name="Email" value="{{ .Email }}">
  </form>
{{ end }}
```
---