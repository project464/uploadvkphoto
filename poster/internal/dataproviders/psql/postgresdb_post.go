package psql

import (
	"gorm.io/gorm"
	"poster/utils"
	"time"
)

func (ip *PostgresProvider) CreatePost(mp *Post) (*Post, error) {
	ip.logger.Info("saving")
	ip.logger.Info("conn:", conn)
	ip.logger.Info("mn:", mp)
	err := conn.Create(mp)
	if err.Error != nil {
		return nil, err.Error
	}
	return mp, nil
}

func (ip *PostgresProvider) GetAllPosts(user User) (posts *[]Post, err error) {
	res := conn.Find(&posts, "owner_id = ?", user.Id)
	if res.Error != nil {
		return nil, res.Error
	}
	return posts, nil
}

func (ip *PostgresProvider) GetAllPostsByTimeWithNoDone(timeOfPost time.Time) (posts *[]Post, err error) {
	res := conn.Where("time_of_post = ? and done = ?", timeOfPost.Format(utils.TIMELAYOUT), false).Find(&posts)
	if res.Error != nil {
		return nil, res.Error
	}
	return posts, nil
}

func (ip *PostgresProvider) GetPosts(mn Post) ([]Post, error) {
	var mns []Post
	ip.logger.Info("Getting posts", mn)
	var res *gorm.DB
	res = conn.Find(&mns, mn)
	if res.Error != nil {
		return nil, res.Error
	}
	return mns, nil
}

func (ip *PostgresProvider) UpdatePost(mn Post) (Post, error) {
	ip.logger.Info("update")
	ip.logger.Info(mn)
	res := conn.Model(&mn).Updates(mn)
	if res.Error != nil {
		return Post{}, res.Error
	}
	return mn, nil
}

func (ip *PostgresProvider) DeletePost(n Post) error {
	var mn Post
	ip.logger.Info("delete")
	ip.logger.Info(n)
	res := conn.Delete(&mn, n)
	if res.Error != nil {
		//log.Println("delete fail")
		return res.Error
	}
	return nil
}
