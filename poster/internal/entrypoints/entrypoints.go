package entrypoints

import (
	"context"
	"go.uber.org/zap"
	"poster/internal/dataproviders/psql"
	"poster/internal/usecases"
	"poster/utils"
	"time"
)

type Entrypoints struct {
	ctx        context.Context
	usecases   usecases.IUseCases
	postgresdb psql.IPostgresProvider
	logger     *zap.SugaredLogger
}

type IEntrypoints interface {
	PostPublicator()
}

func NewEntrypoints(
	ctx context.Context,
	usecases usecases.IUseCases,
	postgresdb psql.IPostgresProvider,
	sugaredlogger *zap.SugaredLogger,
) IEntrypoints {
	return &Entrypoints{
		ctx:        ctx,
		usecases:   usecases,
		postgresdb: postgresdb,
		logger:     sugaredlogger,
	}
}

// PostPublicator забираем все посты, которые должны быть опубликованны
func (ie *Entrypoints) PostPublicator() {
	location, _ := time.LoadLocation("Europe/Moscow")
	timeToPost := time.Now().In(location)
	//date := "2023-03-04 16:00"
	//timeToPost, _ := time.Parse(utils.TIMELAYOUT, date)
	ie.logger.Info("Looking new posts for publication. Time: ", timeToPost.Format(utils.TIMELAYOUT))
	posts := ie.usecases.PostsForPublication(timeToPost)
	ie.logger.Info("Posts: ", len(*posts))

	for _, post := range *posts {
		user, err := ie.postgresdb.GetUser(
			&psql.User{Id: post.OwnerId},
		)
		if err != nil {
			ie.logger.Error(err)
			return
		}
		err = ie.usecases.CacheActuator(user)
		if err != nil {
			ie.logger.Error(err)
			return
		}
		vkDone, err := ie.usecases.PostToVk(user, &post)
		if err != nil {
			ie.logger.Error(err)
		}
		tgDone, err := ie.usecases.PostToTelegram(user, &post)
		if err != nil {
			ie.logger.Error(err)
		}
		if vkDone || tgDone {
			post.Done = true
			_, err = ie.postgresdb.UpdatePost(post)
			if err != nil {
				ie.logger.Error(err)
			}
		}
	}
}
