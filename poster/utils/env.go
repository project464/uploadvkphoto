package utils

import (
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

const (
	TIMELAYOUT = "2006-01-02 15:04"
)

type EnvData struct {
	POSTGRES_HOST string
	POSTGRES_DB   string
	POSTGRES_USER string
	POSTGRES_PASS string
	POSTGRES_PORT int64
	LOG_LEVEL     string
}

func Getenv() EnvData {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalf("There is no .env file")
		panic(err)
	} else {
		psqlhost := os.Getenv("POSTGRES_HOST")
		psqldb := os.Getenv("POSTGRES_DB")
		psqluser := os.Getenv("POSTGRES_USER")
		psqlpass := os.Getenv("POSTGRES_PASSWORD")
		psqlportstring := os.Getenv("POSTGRES_PORT")
		loglevel := os.Getenv("LOG_LEVEL")
		if loglevel == "" {
			loglevel = "PROD"
		}

		psqlport, err := strconv.ParseInt(psqlportstring, 10, 0)
		if err != nil {
			log.Fatalf("wrong psql port")
			panic(err)
		}

		if err == nil {
			env := EnvData{
				psqlhost,
				psqldb,
				psqluser,
				psqlpass,
				psqlport,
				loglevel,
			}
			return env
		} else {
			log.Fatal("error, please check .env file", err)
			panic(err)
		}
	}
}

func CheckError(err error) error {
	if err != nil {
		return err
	} else {
		return nil
	}

}
